# Using

You must have valid crontab file like:

```
# Run task
* * * * * wget -t 1 http://service/task/run >> /var/log/cron.log 2>&1

```

## Starting cronning

Create service in rancher / docker composer and added volume:

`<path/to/crontab>/crontab:crontab`

## Example config:

```yaml
version: '2'
services:
  cron:
    image: registry.gitlab.litea.cz/images/cron:v1.0.0-rc.2
    volumes:
    - /home/containersData/clients/litea/prod/pipi/cron/crontab:/crontab
```

## Links

*  https://crontab.guru/
*  https://www.root.cz/man/5/crontab/
