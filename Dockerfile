FROM alpine:3.7

RUN apk add --no-cache dcron wget \
	&& mkdir -m 0644 -p /var/log \
	&& touch /var/log/cron.log

COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]
