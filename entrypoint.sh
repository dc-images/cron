#!/bin/sh

if [ ! -f /crontab ]; then
    echo "Crontab file not found in /crontab"
    exit 1
fi

# Load cron jobs from /crontab
/usr/bin/crontab /crontab

# Start cron daemon
/usr/sbin/crond -f -l 8